#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <time.h>


#define NUM_PARTICLES 100000

int   cube_exploded = 0;
float angle = 0.0; // camera rotation angle

typedef struct vector
{
	double x,y,z	;
}vector;

typedef struct particle
{
	vector position 	;
	double magnitude;
	vector normal 	;
}particle ;



//World Variables

particle points[NUM_PARTICLES]	;
//-------------------------Vector Operations ------------------------------------


double Magnitude(vector v)
{
	double x = v.x	;
	double y = v.y	;
	double z = v.z	;
	double magnitude = (x * x) + (y * y) + (z * z) ;
	magnitude = (double)sqrt(magnitude);
	//printf("Magnitude: %f\n",magnitude);
	return magnitude	;
}

vector Normalize(vector v)
{
	double mag = Magnitude(v)	;
	vector w;

	w.x = v.x / mag	;
	w.y = v.y / mag	;
	w.z = v.z / mag	;

	return w;
}

double dotProduct(vector u, vector v)
{
	return (u.x + v.x) + (u.y + v.y) + (u.z + v.z)	;
}

vector vectorAdd(vector u, vector v)
{
	vector w;
	w.x = u.x + v.x ;
	w.y = u.y + v.y ;
	w.z = u.z + v.z ;

	return w 	;
} 


vector vectorSubtract(vector u, vector v)
{
	vector w;
	w.x = u.x - v.x ;
	w.y = u.y - v.y ;
	w.z = u.z - v.z ;

	return w 	;
} 

vector vectorScalarDivide(vector v, double f)
{
	vector w;
	w.x = v.x / f 	;
	w.y = v.y / f 	;
	w.z = v.z / f 	;

	return w 	;
}
vector vectorScalarMultiply(vector v, double f)
{
	vector w;
	w.x = v.x * f 	;
	w.y = v.y * f 	;
	w.z = v.z * f 	;

	return w 	;
}

vector negative(vector v)
{
	vector w;

	w.x = -v.x;
	w.y = -v.y;
	w.z = -v.z;

	return w;
}

void display(void);


//---------------------------End of Vector Operations------------------------------------
void initParticles()
{
	for(int i = 0 ; i < NUM_PARTICLES ; i++)
	{
		points[i].position.x = 0,0	;
		points[i].position.y = 0.0	;
		points[i].position.z = 0.0	;


		vector direction	;
		points[i].normal.x = (rand() % 100) - 50   ;	
		points[i].normal.y = (rand() % 100) - 50   ;	
		points[i].normal.z = (rand() % 100) - 50 	;	


		points[i].normal = Normalize(points[i].normal)	;

		points[i].normal = vectorScalarDivide(points[i].normal, 4.0)	;
		points[i].magnitude = 0	;
	}
}
//
// Light sources
GLfloat  light0Amb[4] =  { 1.0, 0.6, 0.2, 1.0 };
GLfloat  light0Dif[4] =  { 1.0, 0.6, 0.2, 1.0 };   
GLfloat  light0Spec[4] = { 0.0, 0.0, 0.0, 1.0 };   
GLfloat  light0Pos[4] =  { 0.0, 0.0, 0.0, 1.0 };
GLfloat  light1Amb[4] =  { 0.0, 0.0, 0.0, 1.0 };
GLfloat  light1Dif[4] =  { 1.0, 1.0, 1.0, 1.0 };   
GLfloat  light1Spec[4] = { 1.0, 1.0, 1.0, 1.0 };   
GLfloat  light1Pos[4] =  { 0.0, 5.0, 5.0, 0.0 };




void display (void);
void keyboard (unsigned char, int, int);
void reshape (int, int);
void idle (void);
void explode_cube (void);

void UpdateParticle()
{
	for(int i = 0 ; i < NUM_PARTICLES ; i++)
	{
		points[i].position = vectorAdd(points[i].position, points[i].normal)	;
	}
} 

void setColor(int i)
{
	
	if(points[i].magnitude > 192.0)
	{
		glColor3f(1.0, 1.0,1.0)	;
	}
	else if (points[i].magnitude > 384.0)
	{
		glColor3f(0.8, 0.8,0.8)	;
	}
	else if (points[i].magnitude > 576.0)
	{
		glColor3f(0.6, 0.6,0.6)	;
	}
	else if(points[i].magnitude > 768.0)
	{
		glColor3f(0.4, 0.4,0.4)	;
	}
	else
	{
		glColor3f(0.2, 0.2,0.2)	;
	}
}

void drawParticles()
{
	glPointSize(2.0)	;


	glBegin(GL_POINTS)	;
		for(int i = 0 ; i < NUM_PARTICLES ; i++)
		{
			setColor(i)	;
			glVertex3f(points[i].position.x, points[i].position.y, points[i].position.z)	;
		}
	glEnd()	;

}


void display (void)
{
  int i;

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity ();
//
// Place the camera
  glTranslatef (0.0, 0.0, -10.0);
  glRotatef (angle, 0.0, 1.0, 0.0);
//
// If no explosion, draw cube
  if (!cube_exploded)
  {
    glEnable (GL_LIGHTING);
    glDisable (GL_LIGHT0);
    glEnable (GL_DEPTH_TEST);
    glutSolidCube (1.0);
  }
  else
  {
  	glDisable(GL_LIGHTING)	;
  	glRotatef(0,0,0,0)	;
  	drawParticles()	;
  	UpdateParticle()	;
  }
    glutSwapBuffers ();
	glutPostRedisplay();
}





void keyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
    case ' ':
      explode_cube();
      angle = 0.0	;
      //exit (0);
    break;
    case 27:
      exit (0);
    break;
  }
}





void reshape (int w, int h)
{
  glViewport (0.0, 0.0, (GLfloat) w, (GLfloat) h);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  gluPerspective (45.0, (GLfloat) w / (GLfloat) h, 0.1, 100.0);
  glMatrixMode (GL_MODELVIEW);
}





void idle (void)
{
	if(cube_exploded != 1)
	{
  		angle += 0.3;  /* Always continue to rotate the camera */
	}
  glutPostRedisplay ();
}









void explode_cube(void)
{
  cube_exploded = 1;
  printf ("BOOM!\n");
}




int main (int argc, char *argv[])
{
	initParticles()	;
	srand(time(NULL))	;



  glutInit (&argc, argv);
  glutInitWindowPosition (0, 0);
  glutInitWindowSize (1920, 1080);
  glutInitDisplayMode (GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
  glutCreateWindow ("Particle explosion");
  glutKeyboardFunc (keyboard);
  glutIdleFunc (idle);
  glutDisplayFunc (display);
  glutReshapeFunc (reshape);
  glEnable (GL_LIGHT0);
  glEnable (GL_LIGHT1);
  glLightfv (GL_LIGHT0, GL_AMBIENT, light0Amb);
  glLightfv (GL_LIGHT0, GL_DIFFUSE, light0Dif);
  glLightfv (GL_LIGHT0, GL_SPECULAR, light0Spec);
  glLightfv (GL_LIGHT0, GL_POSITION, light0Pos);
  glLightfv (GL_LIGHT1, GL_AMBIENT, light1Amb);
  glLightfv (GL_LIGHT1, GL_DIFFUSE, light1Dif);
  glLightfv (GL_LIGHT1, GL_SPECULAR, light1Spec);
  glLightfv (GL_LIGHT1, GL_POSITION, light1Pos);
  glLightModelf (GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glEnable (GL_NORMALIZE);
  glutMainLoop ();
  return 0;
}