// Coding Modified by: 			Peter Thobe
//Course:						CS 116b
//Institution:					San Jose State University
//Instructor:					Robert Bruce
//Template code Provided by:	Robert Bruce
//Version: 1.0

#include <GL/gl.h>
#include <GL/glut.h> 
#include <math.h>
#include <stdio.h>
#include <time.h>



#define BALL_POSITION_X 6.0
#define BALL_POSITION_Y -2.0
#define BALL_POSITION_Z 0.0
#define BALL_RADIUS .75

#define TRUE 1
#define FALSE 0

#define NUM_Particles 21
#define NUM_SPRINGS 20

#define SPRING_CONSTANT .75
#define DAMPER_CONSTANT .1
#define POINT_RADIUS 5.0
#define TIME_STEP .2
#define PARTICLE_MASS 1.0

#define NUM_ITERATIONS 3	




// *************************************************************************************
// * GLOBAL variables. Not ideal but necessary to get around limitatins of GLUT API... *
// *************************************************************************************
int pause = TRUE;


//Personal Structures created to manage Particles and Vectors

//Vector Structure to store Vector coordinates
typedef struct vector
{
	float x	;
	float y	;
	float z	;
}vector;


//Particle structure to store necessary components of a particle to simulation
typedef struct particle
{
	float pMass	;			// Particle Mass
	vector position	;		// Partcle Postion
	vector PreviousPosition;	//Previous Position of Particle
	vector accelaration	;		// Velocity

	float pRadius	;		// Particle radius used for Collision Detection
	// Spring force acting on the particle

	//New Particle Class

	int particleUpdate	;	// control used for determining to update particle position
}particle;


// Spring struct necessary for particle rope simulations
typedef struct spring
{
	int end1	;	//
	int end2	;
	float initLength	;
	float springConstant	;
	float damperConstant	;
}spring;


//Personal added Global Variables

vector gravityVector = {0.0 , -0.15, 0.0}	;	// Gravity Vector
vector tempVector = {0.0,0.0,0.0}	;
vector ballPosition = {BALL_POSITION_X,BALL_POSITION_Y, BALL_POSITION_Z}	;

particle points[NUM_Particles]	; // controle points for Rope Simulation

spring springs[NUM_SPRINGS]	;		//Array of springs that will affect the ropes

int currentCounter = 0 ;

//end of structures and added global variable 


//-------------------------Vector Operations ------------------------------------




double Magnitude(vector v)
{
	double x = v.x	;
	double y = v.y	;
	double z = v.z	;
	double magnitude = (x * x) + (y * y) + (z * z) ;
	magnitude = (double)sqrt(magnitude);
	//printf("Magnitude: %f\n",magnitude);
	return magnitude	;
}

vector Normalize(vector v)
{
	double mag = Magnitude(v)	;
	vector w;

	w.x = v.x / mag	;
	w.y = v.y / mag	;
	w.z = v.z / mag	;

	return w;
}

double dotProduct(vector u, vector v)
{
	return (u.x + v.x) + (u.y + v.y) + (u.z + v.z)	;
}

vector vectorAdd(vector u, vector v)
{
	vector w;
	w.x = u.x + v.x ;
	w.y = u.y + v.y ;
	w.z = u.z + v.z ;

	return w 	;
} 


vector vectorSubtract(vector u, vector v)
{
	vector w;
	w.x = u.x - v.x ;
	w.y = u.y - v.y ;
	w.z = u.z - v.z ;

	return w 	;
} 

vector vectorScalarDivide(vector v, double f)
{
	vector w;
	w.x = v.x / f 	;
	w.y = v.y / f 	;
	w.z = v.z / f 	;

	return w 	;
}
vector vectorScalarMultiply(vector v, double f)
{
	vector w;
	w.x = v.x * f 	;
	w.y = v.y * f 	;
	w.z = v.z * f 	;

	return w 	;
}

vector negative(vector v)
{
	vector w;

	w.x = -v.x;
	w.y = -v.y;
	w.z = -v.z;

	return w;
}

void display(void);


//---------------------------End of Vector Operations------------------------------------



//Start of functions to initialize, update, and draw Particles and springs
void initializeParticles(void)
{
	int multiplier =  -4	;
	for(int i = 0 ; i < NUM_Particles ; i++)
	{
		//initialize Particle mass
		points[i].pMass = PARTICLE_MASS;
		//initialize points starting position
		points[i].position.x = (1 * multiplier)	;
		points[i].position.y = 0.0	;
		points[i].position.z = 0.0	;
		
		//printf("Position vector[%d] x: %f, y: %f, z: %f\n",i,points[i].position.x,points[i].position.y,points[i].position.z)	;
		//Initialize Previous position;
		points[i].PreviousPosition.x = (1 * multiplier)	;
		points[i].PreviousPosition.y = 0.0	;
		points[i].PreviousPosition.z = 0.0	;

		points[i].accelaration.x = 0.0	;
		points[i].accelaration.y = 0.0	;
		points[i].accelaration.z = 0.0	;

		//Set Controle Variable if Initial or ending Point
		if(i == 0 || i == (NUM_Particles - 1))
		{
			points[i].particleUpdate = TRUE	;
		}
		else
		{
			points[i].particleUpdate = FALSE	;
		}
		multiplier++	;
	}

	printf("Ball Position {%f, %f, %f}\n ", ballPosition.x, ballPosition.y,ballPosition.z)	;
}

void initializeSprings(void)
{
	for(int i = 0 ; i < NUM_SPRINGS ; i++)
	{
		springs[i].end1 = i 	;
		springs[i].end2 = i + 1	;
		vector r = vectorSubtract(points[i+1].position, points[i].position) ;
		
		springs[i].initLength = Magnitude( r )	;

		springs[i].springConstant = SPRING_CONSTANT	;
		springs[i].damperConstant = DAMPER_CONSTANT	;

	}
}




void collisionDetection(void)
{
	printf("Point[10], {%f, %f, %f}\n", points[10].position.x, points[10].position.y, points[10].position.z)	;
	double lengthParticleToBall ;
	vector particleToBall, particleToBallNormalized	;

	for(int i = 0 ; i < NUM_Particles ; i++)
	{
		particleToBall = vectorSubtract(points[i].position, ballPosition)	;
		lengthParticleToBall = Magnitude(particleToBall) 	;

		//printf("%f\n ",lengthParticleToBall);

		if(lengthParticleToBall <= BALL_RADIUS)
		{
			particleToBallNormalized = Normalize(particleToBall)	;
			//printf("Point[%d]: Updating Position\n",i)	;

			particleToBallNormalized.x = particleToBallNormalized.x * (BALL_RADIUS - lengthParticleToBall)	;
			particleToBallNormalized.y = particleToBallNormalized.y * (BALL_RADIUS - lengthParticleToBall)	;
			particleToBallNormalized.z = particleToBallNormalized.z * (BALL_RADIUS - lengthParticleToBall)	;

			points[i].position = vectorAdd(points[i].position, particleToBallNormalized)	;

			//printf("Point[%d]: [%f, %f, %f]\n",i, points[i].position.x,points[i].position.y, points[i].position.z)	;
		}
	}
}




void calculateAcceleration(void)
{
	for(int i =0 ; i < NUM_Particles ; i++)
	{
		points[i].accelaration = vectorAdd(points[i].accelaration, gravityVector)	;
	}

	// Add Wind Force to here
}

void satisfyParticleConstraints(void)
{
	currentCounter = 0	;
	while(currentCounter < NUM_ITERATIONS)
	{
		for(int i = 0 ; i < NUM_SPRINGS ; i++)
		{
			int p1, p2	;
			p1 = springs[i].end1	;
			p2 = springs[i].end2	;

			float springRestLength = springs[i].initLength	;
			vector distance 	;

			distance = vectorSubtract(points[p2].position, points[p1].position)	;
			double distanceMagnitude = Magnitude(distance)	;

			double correctVectorFloatPart = 1.0 - (springRestLength / distanceMagnitude)	;
			vector correctionVector = vectorScalarMultiply(distance, correctVectorFloatPart)	;

			correctionVector = vectorScalarMultiply(correctionVector, 0.5);
			
			if(points[p1].particleUpdate != TRUE)
			{
				points[p1].position = vectorAdd( points[p1].position, correctionVector)	;
				
			}
			if (points[p2].particleUpdate != TRUE)
			{
				points[p2].position = vectorSubtract( points[p2].position, correctionVector )	;
			}

			collisionDetection()	;
		}

		currentCounter++	;
	}
}

void computeParticleDisplacement(void)
{
	for(int i = 0 ; i < NUM_Particles ; i++)
	{
		if(points[i].particleUpdate == FALSE)
		{
			tempVector.x = points[i].position.x 	;
			tempVector.y = points[i].position.y 	;
			tempVector.z = points[i].position.z 	;

			vector tempAcceleration = points[i].accelaration	;
			tempAcceleration = vectorScalarMultiply(tempAcceleration, TIME_STEP * TIME_STEP)	;

			vector diffPosition = vectorSubtract(points[i].position, points[i].PreviousPosition)	;

			diffPosition = vectorScalarMultiply(diffPosition, 1.0 - DAMPER_CONSTANT)	;

			diffPosition = vectorAdd(diffPosition, tempAcceleration)	;

			points[i].position = vectorAdd(points[i].position, diffPosition)	;

			points[i].PreviousPosition.x = tempVector.x 	;
			points[i].PreviousPosition.y = tempVector.y 	;
			points[i].PreviousPosition.z = tempVector.z 	;

			//printf("Position of particle 0: x:%f y:%f z: %f\n", points[0].position.x ,points[0].position.y, points[0].position.z)	;

			//reset Particle Acceleration
			points[i].accelaration.x = 0.0	;
			points[i].accelaration.y = 0.0	;
			points[i].accelaration.z = 0.0	;

		}
	}

	for(int i = 0 ; i < NUM_Particles ; i++)
	{

	}
}




void drawRope(void)
{
	glPointSize(POINT_RADIUS);
	glColor3f (1.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS);
		for(int i = 0 ; i < NUM_Particles ; i++)
		{
			glVertex3f(points[i].position.x, points[i].position.y, points[i].position.z)	;
		}
	glEnd();

	glColor3f (0.0f, 1.0f, 0.0f);
	glLineWidth(1.0f)	;
	glBegin(GL_LINES);
		for(int i = 0 ; i < NUM_SPRINGS ; i++)
		{
			glVertex3f(points[springs[i].end1].position.x, points[springs[i].end1].position.y, points[springs[i].end1].position.z)	;
			glVertex3f(points[springs[i].end2].position.x, points[springs[i].end2].position.y, points[springs[i].end2].position.z)	;
		}
	glEnd();

}



//end of custom functions



void init (void)
{
  glShadeModel (GL_SMOOTH);
  glClearColor (0.2f, 0.2f, 0.4f, 0.5f);				
  glClearDepth (1.0f);
  glEnable (GL_DEPTH_TEST);
  glDepthFunc (GL_LEQUAL);
  glEnable (GL_COLOR_MATERIAL);
  glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT0);
  GLfloat lightPos[4] = {-1.0, 1.0, 0.5, 0.0};
  glLightfv (GL_LIGHT0, GL_POSITION, (GLfloat *) &lightPos);
  glEnable (GL_LIGHT1);
  GLfloat lightAmbient1[4] = {0.0, 0.0,  0.0, 0.0};
  GLfloat lightPos1[4]     = {1.0, 0.0, -0.2, 0.0};
  GLfloat lightDiffuse1[4] = {0.5, 0.5,  0.3, 0.0};
  glLightfv (GL_LIGHT1,GL_POSITION, (GLfloat *) &lightPos1);
  glLightfv (GL_LIGHT1,GL_AMBIENT, (GLfloat *) &lightAmbient1);
  glLightfv (GL_LIGHT1,GL_DIFFUSE, (GLfloat *) &lightDiffuse1);
  glLightModeli (GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);


  //Initialize Particles and Springs arrays at start of program
 
}





void display (void)
{
  int x;

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity ();
  glDisable (GL_LIGHTING);
  glBegin (GL_POLYGON);
  glColor3f (0.8f, 0.8f, 1.0f);
  glVertex3f (-200.0f, -100.0f, -100.0f);
  glVertex3f (200.0f, -100.0f, -100.0f);
  glColor3f (0.4f, 0.4f, 0.8f);	
  glVertex3f (200.0f, 100.0f, -100.0f);
  glVertex3f (-200.0f, 100.0f, -100.0f);
  glEnd ();


  //Need to do display before the lighting is enabled for it to appear and Line width needs to be called before begins statement.

  

  
   	

  if(pause == FALSE)
  {
  	calculateAcceleration()	;
 	satisfyParticleConstraints()	;
  	computeParticleDisplacement()	;
  	collisionDetection()	;
  }
 	
  glColor3f (1.0f, 1.0f, 1.0f);

  glEnable (GL_LIGHTING);
  glTranslatef (-6.5, 6.0, -9.0f); // move camera out and center on the rope
  glRotatef(0,0,0,0)	;
  drawRope()	;
  glPushMatrix ();
  glTranslatef (BALL_POSITION_X, BALL_POSITION_Y, BALL_POSITION_Z);
  glColor3f (1.0f, 1.0f, 0.0f);
  glutSolidSphere (BALL_RADIUS - 0.1, 50, 50); // draw the ball, but with a slightly lower radius, otherwise we could get ugly visual artifacts of rope penetrating the ball slightly
  glPopMatrix ();
  glutSwapBuffers();
  glutPostRedisplay();
}





void reshape (int w, int h)  
{
  glViewport (0, 0, w, h);
  glMatrixMode (GL_PROJECTION); 
  glLoadIdentity ();  
  if (h == 0)  
  { 
    gluPerspective (80, (float) w, 1.0, 5000.0);
  }
  else
  {
    gluPerspective (80, (float) w / (float) h, 1.0, 5000.0);
  }
  glMatrixMode (GL_MODELVIEW);  
  glLoadIdentity (); 
}





void keyboard (unsigned char key, int x, int y) 
{
  switch (key) 
  {
    case 27:    
      exit (0);
    break;  
    case 32:
      pause = 1 - pause;
      break;
    default: 
    break;
  }
}





void arrow_keys (int a_keys, int x, int y) 
{
  switch(a_keys) 
  {
    case GLUT_KEY_UP:
      glutFullScreen();
    break;
    case GLUT_KEY_DOWN: 
      glutReshapeWindow (1280, 720 );
    break;
    default:
    break;
  }
}





int main (int argc, char *argv[]) 
{
	initializeParticles()	;
  	initializeSprings()	;


  glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH); 
  glutInitWindowSize (1280, 720 ); 
  glutCreateWindow ("Rope simulator");
  init ();
  glutDisplayFunc (display);  
  glutReshapeFunc (reshape);
  glutKeyboardFunc (keyboard);
  glutSpecialFunc (arrow_keys);
  glutMainLoop ();
}
