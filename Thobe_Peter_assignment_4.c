// Coding Modified by: 			Peter Thobe
//Course:						CS 116b
//Institution:					San Jose State University
//Instructor:					Robert Bruce
//Template code Provided by:	Robert Bruce
//Version: 1.0

#include <GL/gl.h>
#include <GL/glut.h> 
#include <math.h>
#include <stdio.h>
#include <time.h>



#define BALL_POSITION_X 6.5
#define BALL_POSITION_Y -7.5
#define BALL_POSITION_Z 0
#define BALL_RADIUS 1.0

#define TRUE 1
#define FALSE 0

#define NUM_Particles 51
#define NUM_SPRINGS 50

#define SPRING_CONSTANT 1.0
#define DAMPER_CONSTANT .7
#define POINT_RADIUS 5.0
#define TIME_STEP .15
#define PARTICLE_MASS 1.0

#define NUM_ITERATIONS 15	




// *************************************************************************************
// * GLOBAL variables. Not ideal but necessary to get around limitatins of GLUT API... *
// *************************************************************************************
int pause = TRUE;


//Personal Structures created to manage Particles and Vectors

//Vector Structure to store Vector coordinates
typedef struct vector
{
	float x	;
	float y	;
	float z	;
}vector;


//Particle structure to store necessary components of a particle to simulation
typedef struct particle
{
	float pMass	;			// Particle Mass
	vector position	;		// Partcle Postion
	vector PreviousPosition;	//Previous Position of Particle
	vector accelaration	;		// Velocity

	float pRadius	;		// Particle radius used for Collision Detection

	int particleUpdate	;	// control used for determining to update particle position
}particle;


// Spring struct necessary for particle rope simulations
typedef struct spring
{
	int end1r	;	//
	int end1c	;
	int end2r	;
	int end2c	;
	float initLength	;
	float springConstant	;
	float damperConstant	;
}spring;


//Personal added Global Variables

vector gravityVector = {0.0 , -0.5, 0.0}	;	// Gravity Vector
vector tempVector = {0.0,0.0,0.0}	;

particle points[NUM_Particles][NUM_Particles]	; // controle points for Rope Simulation
spring Horizontalsprings[NUM_SPRINGS][NUM_SPRINGS]	;		//Array of Horizontalsprings that will affect the ropes
spring Verticlesprings[NUM_SPRINGS][NUM_SPRINGS]	;		//Array of Verticle Springs
spring FDsprings[NUM_SPRINGS][NUM_SPRINGS]	;				//Array of FDsprings(\)
spring BDsprings[NUM_SPRINGS][NUM_SPRINGS]	;				//Array of BDSprings(/)

int currentCounter = 0 ;

//end of structures and added global variable 


//-------------------------Vector Operations ------------------------------------




double Magnitude(vector v)
{
	double x = v.x	;
	double y = v.y	;
	double z = v.z	;
	double magnitude = (x * x) + (y * y) + (z * z) ;
	magnitude = (double)sqrt(magnitude);
	//printf("Magnitude: %f\n",magnitude);
	return magnitude	;
}

vector Normalize(vector v)
{
	double mag = Magnitude(v)	;
	vector w;

	w.x = v.x / mag	;
	w.y = v.y / mag	;
	w.z = v.z / mag	;

	return w;
}

double dotProduct(vector u, vector v)
{
	return (u.x * v.x) + (u.y * v.y) + (u.z * v.z)	;
}

vector vectorAdd(vector u, vector v)
{
	vector w;
	w.x = u.x + v.x ;
	w.y = u.y + v.y ;
	w.z = u.z + v.z ;

	return w 	;
} 


vector vectorSubtract(vector u, vector v)
{
	vector w;
	w.x = u.x - v.x ;
	w.y = u.y - v.y ;
	w.z = u.z - v.z ;

	return w 	;
} 

vector vectorScalarDivide(vector v, double f)
{
	vector w;
	w.x = v.x / f 	;
	w.y = v.y / f 	;
	w.z = v.z / f 	;

	return w 	;
}
vector vectorScalarMultiply(vector v, double f)
{
	vector w;
	w.x = v.x * f 	;
	w.y = v.y * f 	;
	w.z = v.z * f 	;

	return w 	;
}

vector negative(vector v)
{
	vector w;

	w.x = -v.x;
	w.y = -v.y;
	w.z = -v.z;

	return w;
}


void display(void);


//---------------------------End of Vector Operations------------------------------------






//------Start of functions to initialize, update, and draw Particles and springs
void initializeParticles(void) //BORKBORKBORK
{
	
	double Ymultiplier = 12.5	;

	for(int i = 0 ; i < NUM_Particles ; i++)
	{	
		
		double Xmultiplier =  -12.5	;
		// printf("X: %f\n",Xmultiplier)	;
		// printf("Y: %f\n",Ymultiplier)	;

		for(int j = 0 ; j < NUM_Particles ; j++)
		{
			//initialize Particle mass
			points[i][j].pMass = PARTICLE_MASS;
			//initialize points starting position
			points[i][j].position.x = Xmultiplier	;
			points[i][j].position.y = Ymultiplier	;
			points[i][j].position.z = -18.0	;
			
			//printf("Position vector[%d] x: %f, y: %f, z: %f\n",i,points[i].position.x,points[i].position.y,points[i].position.z)	;
			//Initialize Previous position;
			points[i][j].PreviousPosition.x = Xmultiplier	;
			points[i][j].PreviousPosition.y = Ymultiplier	;
			points[i][j].PreviousPosition.z = -18.0	;

			points[i][j].accelaration.x = 0.0	;
			points[i][j].accelaration.y = 0.0	;
			points[i][j].accelaration.z = 0.0	;

			//Set Controle Variable if Initial or ending Point
			if((i == 0 && j == 0))
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else if((i == 0 && j == 1))
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else if((i == 1 && j == 0))
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else if((i == 0 && j == 50) )
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else if((i == 0 && j == 49))
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else if(i == 1 && j == 50)
			{
				//printf("{%d,%d}: donot update set to true\n",i,j)	;
				points[i][j].particleUpdate = TRUE	;
			}
			else
			{
				points[i][j].particleUpdate = FALSE	;
			}

			Xmultiplier += 0.5	;
			if(i == 50)
			{
				printf("Point[%d,%d], {%f , %f , %f}\n", i, j, points[i][j].position.x , points[i][j].position.y , points[i][j].position.z)	;
			}
		}

		Ymultiplier-=	0.5;
	}

	// printf("particle 0, 0 ; {%0.1f , %0.1f , %0.1f}\n",points[0][0].position.x,points[0][0].position.y,points[0][0].position.z )	;
	// printf("particle 0, 50 ; {%0.1f , %0.1f , %0.1f}\n",points[0][50].position.x,points[0][50].position.y,points[0][50].position.z )	;
	// printf("particle 50, 0 ; {%0.1f , %0.1f , %0.1f}\n",points[50][0].position.x,points[50][0].position.y,points[50][0].position.z )	;
	// printf("particle 50, 50 ; {%0.1f , %0.1f , %0.1f}\n",points[50][50].position.x,points[50][50].position.y,points[50][50].position.z )	;

}


void initializeSprings(void)
{
	for(int i = 0 ; i < NUM_SPRINGS ; i++)
	{
		for(int j = 0 ; j < NUM_SPRINGS ; j++)
		{
			// horizontal 
			Horizontalsprings[i][j].end1r = i 	;
			Horizontalsprings[i][j].end1c = j 	;

			Horizontalsprings[i][j].end2r = i 	;
			Horizontalsprings[i][j].end2c = j+1	;

			vector r = vectorSubtract(points[i][j+1].position, points[i][j].position) ;
			
			Horizontalsprings[i][j].initLength = Magnitude( r )	;

			Horizontalsprings[i][j].springConstant = SPRING_CONSTANT	;
			Horizontalsprings[i][j].damperConstant = DAMPER_CONSTANT	;


			//Verticle
			Verticlesprings[i][j].end1r = i 	;
			Verticlesprings[i][j].end1c = j 	;

			Verticlesprings[i][j].end2r = i + 1	;
			Verticlesprings[i][j].end2c = j 	;

			r = vectorSubtract(points[i + 1][j].position, points[i][j].position) ;
			
			Verticlesprings[i][j].initLength = Magnitude( r )	;

			Verticlesprings[i][j].springConstant = SPRING_CONSTANT	;
			Verticlesprings[i][j].damperConstant = DAMPER_CONSTANT	;


			//FD springs
			FDsprings[i][j].end1r = i 	;
			FDsprings[i][j].end1c = j 	;

			FDsprings[i][j].end2r = i + 1	;
			FDsprings[i][j].end2c = j + 1	;

			r = vectorSubtract(points[i+1][j+1].position, points[i][j].position) ;
			
			FDsprings[i][j].initLength = Magnitude( r )	;

			FDsprings[i][j].springConstant = SPRING_CONSTANT	;
			FDsprings[i][j].damperConstant = DAMPER_CONSTANT	;


			//BD springs
			BDsprings[i][j].end1r = i + 1	;
			BDsprings[i][j].end1c = j 	;

			BDsprings[i][j].end2r = i 	;
			BDsprings[i][j].end2c = j + 1	;


			r = vectorSubtract(points[i][j + 1].position, points[i + 1][j].position) ;
			
			BDsprings[i][j].initLength = Magnitude( r )	;

			BDsprings[i][j].springConstant = SPRING_CONSTANT	;
			BDsprings[i][j].damperConstant = DAMPER_CONSTANT	;

		}

	}
}


void calculateAcceleration(void)
{
	for(int i =0 ; i < NUM_Particles ; i++)
	{
		for(int j = 0 ; j < NUM_Particles ; j++)
		{
			if(points[i][j].particleUpdate != TRUE)
			{
				points[i][j].accelaration = vectorAdd(points[i][j].accelaration, gravityVector)	;
			}
		}
	}
}

void satisfyParticleConstraints(void)
{
	currentCounter = 0	;
	while(currentCounter < NUM_ITERATIONS)
	{
		for(int i = 0 ; i < NUM_SPRINGS ; i++)
		{
			for(int j = 0 ; j < NUM_SPRINGS ; j++)
			{
				int p1r, p1c, p2r, p2c	;

				//Horizontal Springs
				
				p1r = Horizontalsprings[i][j].end1r	;
				p1c = Horizontalsprings[i][j].end1c	;

				p2r = Horizontalsprings[i][j].end2r	;
				p2c = Horizontalsprings[i][j].end2c	;


				float springRestLength = Horizontalsprings[i][j].initLength	;
				vector distance 	;

				distance = vectorSubtract(points[p2r][p2c].position, points[p1r][p1c].position)	;
				double distanceMagnitude = Magnitude(distance)	;

				double correctVectorFloatPart = 1.0 - (springRestLength / distanceMagnitude)	;
				vector correctionVector = vectorScalarMultiply(distance, correctVectorFloatPart)	;

				correctionVector = vectorScalarMultiply(correctionVector, 0.5);
				
				if(points[p1r][p1c].particleUpdate != TRUE)
				{
					points[p1r][p1c].position = vectorAdd( points[p1r][p1c].position, correctionVector)	;
					
				}
				if (points[p2r][p2c].particleUpdate != TRUE)
				{
					points[p2r][p2c].position = vectorSubtract( points[p2r][p2c].position, correctionVector )	;
				}

				//Verticle
				p1r = Verticlesprings[i][j].end1r	;
				p1c = Verticlesprings[i][j].end1c	;

				p2r = Verticlesprings[i][j].end2r	;
				p2c = Verticlesprings[i][j].end2c	;


				springRestLength = Verticlesprings[i][j].initLength	;

				distance = vectorSubtract(points[p2r][p2c].position, points[p1r][p1c].position)	;
				distanceMagnitude = Magnitude(distance)	;

				correctVectorFloatPart = 1.0 - (springRestLength / distanceMagnitude)	;
				correctionVector = vectorScalarMultiply(distance, correctVectorFloatPart)	;

				correctionVector = vectorScalarMultiply(correctionVector, 0.5);
				
				if(points[p1r][p1c].particleUpdate != TRUE)
				{
					points[p1r][p1c].position = vectorAdd( points[p1r][p1c].position, correctionVector)	;
					
				}
				if (points[p2r][p2c].particleUpdate != TRUE)
				{
					points[p2r][p2c].position = vectorSubtract( points[p2r][p2c].position, correctionVector )	;
				}

				//FD Springs

				p1r = FDsprings[i][j].end1r	;
				p1c = FDsprings[i][j].end1c	;

				p2r = FDsprings[i][j].end2r	;
				p2c = FDsprings[i][j].end2c	;


				springRestLength = FDsprings[i][j].initLength	;
				distance 	;

				distance = vectorSubtract(points[p2r][p2c].position, points[p1r][p1c].position)	;
				distanceMagnitude = Magnitude(distance)	;

				correctVectorFloatPart = 1.0 - (springRestLength / distanceMagnitude)	;
				correctionVector = vectorScalarMultiply(distance, correctVectorFloatPart)	;

				correctionVector = vectorScalarMultiply(correctionVector, 0.5);
				
				if(points[p1r][p1c].particleUpdate != TRUE)
				{
					points[p1r][p1c].position = vectorAdd( points[p1r][p1c].position, correctionVector)	;
					
				}
				if (points[p2r][p2c].particleUpdate != TRUE)
				{
					points[p2r][p2c].position = vectorSubtract( points[p2r][p2c].position, correctionVector )	;
				}

				//BD Springs

				p1r = BDsprings[i][j].end1r	;
				p1c = BDsprings[i][j].end1c	;

				p2r = BDsprings[i][j].end2r	;
				p2c = BDsprings[i][j].end2c	;


				springRestLength = BDsprings[i][j].initLength	;
				distance 	;

				distance = vectorSubtract(points[p2r][p2c].position, points[p1r][p1c].position)	;
				distanceMagnitude = Magnitude(distance)	;

				correctVectorFloatPart = 1.0 - (springRestLength / distanceMagnitude)	;
				correctionVector = vectorScalarMultiply(distance, correctVectorFloatPart)	;

				correctionVector = vectorScalarMultiply(correctionVector, 0.5);
				
				if(points[p1r][p1c].particleUpdate != TRUE)
				{
					points[p1r][p1c].position = vectorAdd( points[p1r][p1c].position, correctionVector)	;
					
				}
				if (points[p2r][p2c].particleUpdate != TRUE)
				{
					points[p2r][p2c].position = vectorSubtract( points[p2r][p2c].position, correctionVector )	;
				}
			}
		}

		currentCounter++	;
	}
}

void computeParticleDisplacement(void)
{
	for(int i = 0 ; i < NUM_Particles ; i++)
	{
		for(int j = 0 ; j < NUM_Particles ; j++)
		{		
			if(points[i][j].particleUpdate == FALSE)
			{
				tempVector.x = points[i][j].position.x 	;
				tempVector.y = points[i][j].position.y 	;
				tempVector.z = points[i][j].position.z 	;

				vector tempAcceleration = points[i][j].accelaration	;
				tempAcceleration = vectorScalarMultiply(tempAcceleration, TIME_STEP * TIME_STEP)	;

				vector diffPosition = vectorSubtract(points[i][j].position, points[i][j].PreviousPosition)	;

				diffPosition = vectorScalarMultiply(diffPosition, 1.0 - DAMPER_CONSTANT)	;

				diffPosition = vectorAdd(diffPosition, tempAcceleration)	;

				points[i][j].position = vectorAdd(points[i][j].position, diffPosition)	;

				points[i][j].PreviousPosition.x = tempVector.x 	;
				points[i][j].PreviousPosition.y = tempVector.y 	;
				points[i][j].PreviousPosition.z = tempVector.z 	;

				//printf("Position of particle 0: x:%f y:%f z: %f\n", points[0].position.x ,points[0].position.y, points[0].position.z)	;

				//reset Particle Acceleration
				points[i][j].accelaration.x = 0.0	;
				points[i][j].accelaration.y = 0.0	;
				points[i][j].accelaration.z = 0.0	;
			}
		}
	}
}




void drawRope(void)
{
	int p1r , p1c	;
	int p2r , p2c	;
	int p3r , p3c	;
	glColor3f(0.0, 1.0, 0.0)	;
	for(int i = 0 ; i < NUM_Particles-1 ; i++)
	{
		for(int j = 0 ; j < NUM_Particles-1; j++)
		{
			p1r = i 	;
			p1c = j 	;

			p2r = i 	;
			p2c = j + 1	;

			p3r = i + 1 	;
			p3c = j 	;
			
			glBegin(GL_TRIANGLES)	;
				glVertex3f(points[p1r][p1c].position.x, points[p1r][p1c].position.y, points[p1r][p1c].position.z)	;

				glVertex3f(points[p2r][p2c].position.x, points[p2r][p2c].position.y, points[p2r][p2c].position.z)	;

				glVertex3f(points[p3r][p3c].position.x, points[p3r][p3c].position.y, points[p3r][p3c].position.z)	;
				
			glEnd()	;
		}
	}


	glColor3f(0.0, 0.0, 1.0)	;
	
	for(int i = 0 ; i < NUM_Particles-1 ; i++)
	{
		for(int j = 0 ; j < NUM_Particles-1; j++)
		{
			
			p1r = i + 1	;
			p1c = j 	;

			p2r = i 	;
			p2c = j + 1	;

			p3r = i + 1	;
			p3c = j + 1	;
			
			glBegin(GL_TRIANGLES)	;
				glVertex3f(points[p1r][p1c].position.x, points[p1r][p1c].position.y, points[p1r][p1c].position.z)	;

				glVertex3f(points[p2r][p2c].position.x, points[p2r][p2c].position.y, points[p2r][p2c].position.z)	;

				glVertex3f(points[p3r][p3c].position.x, points[p3r][p3c].position.y, points[p3r][p3c].position.z)	;
				
			glEnd()	;
		}
	}
}



//end of custom functions



void init (void)
{
  glShadeModel (GL_SMOOTH);
  glClearColor (0.0f, 0.0f, 0.0f, 0.5f);				
  glClearDepth (1.0f);
  glEnable (GL_DEPTH_TEST);
  glDepthFunc (GL_LEQUAL);
  glEnable (GL_COLOR_MATERIAL);
  glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT0);
  GLfloat lightPos[4] = {-1.0, 1.0, 0.5, 0.0};
  glLightfv (GL_LIGHT0, GL_POSITION, (GLfloat *) &lightPos);
  glEnable (GL_LIGHT1);
  GLfloat lightAmbient1[4] = {0.0, 0.0,  0.0, 0.0};
  GLfloat lightPos1[4]     = {1.0, 0.0, -0.2, 0.0};
  GLfloat lightDiffuse1[4] = {0.5, 0.5,  0.3, 0.0};
  glLightfv (GL_LIGHT1,GL_POSITION, (GLfloat *) &lightPos1);
  glLightfv (GL_LIGHT1,GL_AMBIENT, (GLfloat *) &lightAmbient1);
  glLightfv (GL_LIGHT1,GL_DIFFUSE, (GLfloat *) &lightDiffuse1);
  glLightModeli (GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);


  //Initialize Particles and Springs arrays at start of program
 
}



void display (void)
{
  int x;

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity ();
  glDisable (GL_LIGHTING);
  // glBegin (GL_POLYGON);
  // glColor3f (0.8f, 0.8f, 1.0f);
  // glVertex3f (-200.0f, -100.0f, -100.0f);
  // glVertex3f (200.0f, -100.0f, -100.0f);
  // glColor3f (0.4f, 0.4f, 0.8f);	
  // glVertex3f (200.0f, 100.0f, -100.0f);
  // glVertex3f (-200.0f, 100.0f, -100.0f);
  // glEnd ();

  //Need to do display before the lighting is enabled    	

  if(pause == FALSE)
  {
  	calculateAcceleration()	;
 	satisfyParticleConstraints()	;
  	computeParticleDisplacement()	;
  }
 	drawRope()	;

  glColor3f (1.0f, 1.0f, 1.0f);

  //glEnable (GL_LIGHTING);
  glTranslatef (-6.5, 6, -10.0f); // move camera out and center on the rope was -10
  glPushMatrix ();
  glTranslatef (BALL_POSITION_X, BALL_POSITION_Y, BALL_POSITION_Z);
  glColor3f (1.0f, 1.0f, 0.0f);
  //glutSolidSphere (BALL_RADIUS - 0.1, 50, 50); // draw the ball, but with a slightly lower radius, otherwise we could get ugly visual artifacts of rope penetrating the ball slightly
  glPopMatrix ();

  glutSwapBuffers();
  glutPostRedisplay();
}





void reshape (int w, int h)  
{
  glViewport (0, 0, w, h);
  glMatrixMode (GL_PROJECTION); 
  glLoadIdentity ();  
  if (h == 0)  
  { 
    gluPerspective (80, (float) w, 1.0, 5000.0);
  }
  else
  {
    gluPerspective (80, (float) w / (float) h, 1.0, 5000.0);
  }
  glMatrixMode (GL_MODELVIEW);  
  glLoadIdentity (); 
}





void keyboard (unsigned char key, int x, int y) 
{
  switch (key) 
  {
    case 27:    
      exit (0);
    break;  
    case 32:
      pause = 1 - pause;
      break;
    default: 
    break;
  }
}



void arrow_keys (int a_keys, int x, int y) 
{
  switch(a_keys) 
  {
    case GLUT_KEY_UP:
      glutFullScreen();
    break;
    case GLUT_KEY_DOWN: 
      glutReshapeWindow (1920, 1080 );
    break;
    default:
    break;
  }
}


int main (int argc, char *argv[]) 
{
	initializeParticles()	;
  	initializeSprings()	;


  glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH); 
  glutInitWindowSize (1920, 1080 ); 
  glutCreateWindow ("Rope simulator");
  init ();
  glutDisplayFunc (display);  
  glutReshapeFunc (reshape);
  glutKeyboardFunc (keyboard);
  glutSpecialFunc (arrow_keys);
  glutMainLoop ();
}
